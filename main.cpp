#include <iostream>
#include <string>
#include <exception>

using namespace std;

template<typename T>
class Bag {

private:
    int size;
public:
    int i = 0;
    T *arr;

    Bag(T value, int num) {
        size = num;
        arr = new T[size];
    }

    ~Bag() {
        delete[] arr;
        arr = nullptr;
    }

    T Add(T value_2) {
        if (i >= size) {
            throw ("too big");
        } else {
            *(arr + i) = value_2;
            i++;
            return value_2;
        }

    }

    T operator[](int index) {
        if (index >= size) {
            throw ("wrong index");
        } else
            return *(arr + index);
    }
};

int main() {

    Bag<int> bag(5, 3);
    try {
        bag.Add(1);
    }
    catch (char const *e) {
        cout << e << endl;
    }
    try {
        bag.Add(21);
    }
    catch (char const *e) {
        cout << e << endl;
    }
    try {
        bag.Add(31);
    }
    catch (char const *e) {
        cout << e << endl;
    }
    try {
        bag.Add(41);
    }
    catch (char const *e) {
        cout << e << endl;
    }

    try {
        cout << bag[1] << endl;
    }
    catch (char const *e) {
        cout << e << endl;
    }
    try {
        cout << bag[100] << endl;
    }
    catch (char const *e) {
        cout << e << endl;
    }

   
    return 0;
}
